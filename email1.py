import naas_drivers
import naas


spreadsheet_id = "1NyuYM0ructGHvh-vBzed7h0kAM5nlyDN-lIYcBJ5Loo"
data = naas_drivers.gsheet.connect(spreadsheet_id).get(sheet_name="Tasks")
data

your_email = "madikuntarya@gmail.com" #sender's email
name_list = data['Name']
email_list = data['Email']

url_image = naas.assets.add("Smile.gif")

email_content = naas_drivers.html.generate(
    display = 'iframe',
    title="Internship Task",
    heading="Good Day {name}",
    image=f"{url_image}",
    text_1="adfghjk",
    text_2="sdfghjk"
)

for i in range(len(data)):
    subject="Internship Task"
    email_to = email_list[i]
    name = name_list[i]
    content = email_content.replace("{name}", name)
    naas.notifications.send(email_to = email_to, subject = subject, html = content, email_from = your_email)
